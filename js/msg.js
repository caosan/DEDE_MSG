/**
 * Created by caoSan on 2015/11/13.
 */
$(function() {
    //写入
    $('#msg').validate({
        submitHandler : function(form) {
            $(form).ajaxSubmit({
                url : './include/usermsg_insert.php',
                type : 'POST',
                beforeSubmit : function() {
                    $('#msgsubmit').prop('disabled', true).val('发送中...').css('background', '#6cf');
                },
                success : function (responseText) {
                    if(responseText == 1) {
                        $('#msgsubmit').val('成功').css('background', 'green');
                        setTimeout(function(){
                            $('#msg').resetForm();
                            location.href = "./message.php";
                        }, 500);
                    } else {
                        switch(responseText) {
                            case '-7' :
                                errorReset('内容长度错误')
                                break;
                        }
                    }
                },
            });
        },
        //错误高亮
        highlight : function(element, errorClass) {
            $(element).css('border','1px solid #f60');
            $(element).parent().find('span').remove();
        },
        //取消高亮
        unhighlight : function(element, errorClass) {
            $(element).css('border','1px solid #8DB800');
            $(element).parent().find('span').remove();
        },
        //验证规则
        rules : {
            msguser : {
                required : true,
                minlength : 2,
                maxlength : 20,
            },
            msgtel: {
                required : true,
                isTel : true,
            },
            msgemail : {
                email : true,
            },
            msgcon : {
                required : true,
                minlength : 10,
                maxlength : 250,
            },
        },
        messages : {
            msguser : {
                required : '必填',
                minlength : $.validator.format('最小{0}位'),
                maxlength : $.validator.format('最大{0}位'),
            },
            msgtel: {
                required : '必填',
            },
            msgemail : {
                email : '请填写正确的邮箱格式',
            },
            msgcon : {
                required : '必填',
                minlength : $.validator.format('最小{0}位'),
                maxlength : $.validator.format('最大{0}位'),
            },
        },
    });
    // 联系电话(手机/电话皆可)验证
    jQuery.validator.addMethod("isTel", function(value,element) {
        var length = value.length;
        var mobile = /^(((13[0-9]{1})|(15[0-9]{1})|(18[0-9]{1}))+\d{8})$/;
        var tel = /^(\d{3,4}-?)?\d{7,9}$/g;
        return this.optional(element) || tel.test(value) || (length==11 && mobile.test(value));
    }, "请正确填写您的联系方式");

    function errorReset(info) {
        $('.returnError').html(info + '!请修改后重试!').show();
        $('#msgsubmit').prop('disabled', false).val('发送').css('background', '#0B78A4');
        setTimeout(function() {
            $('.returnError').hide().html('');
        },1500);
    }
});