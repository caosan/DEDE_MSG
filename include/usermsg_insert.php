<?php
/**
 * Created by PhpStorm.
 * User: caoSan
 * Date: 2015/11/13
 * Time: 17:26
 */
header('Content-type:text/html;charset=utf8');

/**
 * 错误代码
 * -1 用户名为空
 * -2 用户名长度不符
 * -3 联系方式不存在
 * -4 联系方式格式不正确
 * -5 邮箱格式不正确
 * -6 内容不存在
 * -7 内容长度不符
 */
//如果通过ajax发送
if(isset($_SERVER["HTTP_X_REQUESTED_WITH"]) && strtolower($_SERVER["HTTP_X_REQUESTED_WITH"])=="xmlhttprequest") {
    sleep(1);
    /**
     * 接收POST数据
     */
    //不为空 长度2-20
    $msguser = strip_tags($_POST['msguser']);
    //不为空 电话格式
    $msgtel = strip_tags($_POST['msgtel']);
    //可为空 email格式
    $msgemail = strip_tags($_POST['msgemail']);
    //不为空 长度10-250
    $msgcon = strip_tags($_POST['msgcon']);

    //定义错误变量
    $errorCode = 0;
    //判断昵称是否存在
    //不存在返回-1
    if(!$msguser) {
        $errorCode = -1;
        echo $errorCode;
        exit();
    } else {
        //存在判断长度
        //不符返回-2
        $len = mb_strlen($msguser, 'utf-8');
        if($len<2 || $len>20) {
            $errorCode = -2;
            echo $errorCode;
            exit();
        }
    }
    //判断联系方式是否存在
    //不存在返回-3
    if(!$msgtel) {
        $errorCode = -3;
        echo $errorCode;
        exit();
    } else {
        //存在判断格式
        $len = mb_strlen($msgtel, 'utf-8');
        $mobile = '/^(((13[0-9]{1})|(15[0-9]{1})|(18[0-9]{1}))+\d{8})$/';
        $tel = '/^(\d{3,4}-?)?\d{7,9}$/';
        //判断正则是否匹配
        if((!preg_match($mobile, $msgtel)) and (!preg_match($tel, $msgtel))) {
            //不符返回-4
            $errorCode = -4;
            echo $errorCode;
            exit();
        }
    }
    //判断email格式
    if($msgemail) {
        $email = '/^[a-zA-Z0-9][a-zA-Z0-9._-]*\@[a-zA-Z0-9]+\.[a-zA-Z0-9\.]+$/A';
        if(!preg_match($email, $msgemail)) {
            //不符返回-5
            $errorCode = -5;
            echo $errorCode;
            exit();
        }
    }
    //判断内容
    //不存在返回-6
    if(!$msgcon) {
        $errorCode = -6;
        echo $errorCode;
        exit();
    } else {
        //存在判断长度
        //不符返回-7
        $len = mb_strlen($msgcon, 'utf-8');
        if($len<10 || $len>250) {
            $errorCode = -7;
            echo $errorCode;
            exit();
        }
    }

    //全部通过 写入数据库
    if($errorCode == 0) {
        include "../data/common.inc.php";
        //创建对象并打开连接，最后一个参数是选择的数据库名称
        $mysqli = new mysqli($cfg_dbhost,$cfg_dbuser,$cfg_dbpwd,$cfg_dbname);
        //检查连接是否成功
        if (mysqli_connect_errno()){
            //注意mysqli_connect_error()新特性
            die('数据库连接失败!'). mysqli_connect_error();
        }
        if (!$mysqli->set_charset("utf8")) {
            printf("字符集设置错误\n", $mysqli->error);
        }
        $ip = $_SERVER['REMOTE_ADDR'];
        $sql = "INSERT INTO dede_msg VALUES (null,'$msguser','$msgtel','$msgemail','$msgcon',now(),'$ip',null,null)";
        //执行sql语句，完全面向对象的
        $result = $mysqli->query($sql);
        $mysqli->close();
        echo $result;
    }
}
