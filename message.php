<?php
define('ALLOW', true);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Msg</title>
    <link rel="stylesheet" href="http://xzrcyy.com/style/global.css" />
    <link rel="stylesheet" href="http://xzrcyy.com/style/list.css" />
    <link rel="stylesheet" href="./style/msg.css" />
</head>
<body>
<div class="w1000">
    <div id="content">
        <div class="w1000 clearfix" id="now">当前位置: <a href="http://b.xz39.com/">首页</a> / <a href="/plus/list.php?tid=9">在线留言</a> / </div>
        <?php
            //如果action不存在 或者action==list
            if((!isset($_GET['action'])) || (isset($_GET['action']) && $_GET['action'] == 'list')) {
        ?>
        <div class="bd">
            <?php include "./include/usermsg_select.php"?>
        </div>
        <?php
            }
        ?>
        <?php
            if(isset($_GET['action']) && $_GET['action'] == 'add') {
        ?>
        <script src="http://xzrcyy.com/js/jquery.min.js"></script>
        <script src="./js/msg.js"></script>
        <script src="./js/jquery.form.min.js"></script>
        <script src="./js/jquery.validate.min.js"></script>
        <div class="bd">
            <form id="msg">
                <div>
                    <label for="msguser">昵称 : </label>
                    <input type="text" id="msguser" name="msguser" />
                </div>
                <div>
                    <label for="msgtel">联系方式 : </label>
                    <input type="text" id="msgtel" name="msgtel" />
                </div>
                <div>
                    <label for="msgemail">Email : </label>
                    <input type="text" id="msgemail" name="msgemail" />
                </div>
                <div>
                    <label for="msgcon">内容 : </label>
                    <textarea id="msgcon" name="msgcon"></textarea>
                </div>
                <div>
                    <input type="submit" id="msgsubmit" value="发送" />
                    <input type="reset" value="重置" />
                </div>
                <div class="returnError"></div>
            </form>
        </div>
        <?php
            }
        ?>
    </div>
    <div id="sidebar">
        <div class="msgtitle">
            留言咨询
        </div>
        <div class="msgnav">
            <a href="?action=list" <?php if((!isset($_GET['action'])) || (isset($_GET['action']) && $_GET['action'] == 'list')) echo "class='on'"?>>留言列表</a>
            <a href="?action=add" <?php if(isset($_GET['action']) && $_GET['action'] == 'add') echo "class='on'"?>>我要留言</a>
        </div>
    </div>
</div>
</body>
</html>