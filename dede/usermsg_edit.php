<?php
/**
 * 留言编辑
 */
require_once(dirname(__FILE__)."/config.php");
//CheckPurview('plus_留言编辑模块');
$ENV_GOBACK_URL = empty($_COOKIE['ENV_GOBACK_URL']) ? 'usermsg_main.php' : $_COOKIE['ENV_GOBACK_URL'];
if(empty($dopost)) $dopost = "";
if(isset($allid))
{
    $aids = explode(',',$allid);
    if(count($aids)==1)
    {
        $id = $aids[0];
        $dopost = "delete";
    }
}
if($dopost=="delete")
{
    $id = preg_replace("#[^0-9]#", "", $id);
    $dsql->ExecuteNoneQuery("DELETE FROM dede_msg WHERE id='$id'");
    ShowMsg("成功删除一个留言！",$ENV_GOBACK_URL);
    exit();
}
else if($dopost=="delall")
{
    $aids = explode(',',$aids);
    if(isset($aids) && is_array($aids))
    {
        foreach($aids as $aid)
        {
            $aid = preg_replace("#[^0-9]#", "", $aid);
            $dsql->ExecuteNoneQuery("DELETE FROM dede_msg WHERE id='$aid'");
        }
        ShowMsg("成功删除指定留言！",$ENV_GOBACK_URL);
        exit();
    }
    else
    {
        ShowMsg("你没选定任何留言！",$ENV_GOBACK_URL);
        exit();
    }
}
else if($dopost=="saveedit")
{
	require_once DEDEINC.'/request.class.php';
	$request = new Request();
	$request->Init();
    $id = preg_replace("#[^0-9]#", "", $request->Item('id', 0));

    $username = $request->Item('username', '');
    $tel = $request->Item('tel', '');
    $email = $request->Item('email', '');
    $content = $request->Item('content', '');
    $time = $request->Item('time', '');
    $create = $request->Item('create', '');
    $reply = $request->Item('reply', '');
    $replytime = $request->Item('replytime', '');
    if(!$replytime || $replytime == '0000-00-00 00:00:00') {
        $replytime = date("Y-m-d H:i:s");
    }
    $query = "UPDATE dede_msg SET username='$username',tel='$tel',email='$email',content='$content',`time`='$time',`create`='$create',reply='$reply',replytime='$replytime' WHERE id='$id' ";
    $dsql->ExecuteNoneQuery($query);
    ShowMsg("成功回复一个留言！",$ENV_GOBACK_URL);
    exit();
}
$myLink = $dsql->GetOne("SELECT * FROM dede_msg WHERE id='$id'");
include DedeInclude('templets/usermsg_edit.html');